package main

import (
	"flag"
	"fmt"
	"github.com/micmonay/keybd_event"
	"io/ioutil"
	"log"
	"strings"
	"time"
	"unicode"
)

var (
	keys = map[string]int{
		"0": 11, "1": 2, "2": 3, "3": 4, "4": 5, "5": 6, "6": 7, "7": 8, "8": 9, "9": 10, "a": 30, "b": 48,
		"c": 46, "d": 32, "e": 18, "f": 33, "g": 34, "h": 35, "i": 23, "j": 36, "k": 37, "l": 38, "m": 50, "n": 49,
		"o": 24, "p": 25, "q": 16, "r": 19, "s": 31, "t": 20, "u": 22, "v": 47, "w": 17, "x": 45, "y": 21, "z": 44,
		"-": 12, "=": 13, "[": 26, "]": 27, ";": 39, "'": 40, "`": 41, "\\": 43, ",": 51, ".": 52, "/": 53, " ": 57,
		"end": 107, "left": 105, "right": 106, "tab": 15, "backspace": 14, "newline": 28,
	}
	switches = map[string]string{
		"~": "`", "!": "1", "@": "2", "#": "3", "$": "4", "%": "5", "^": "6", "&": "7", "*": "8", "(": "9", ")": "0",
		"_": "-", "+": "=", "{": "[", "}": "]", "|": "\\", ":": ";", `"`: "'", "<": ",", ">": ".", "?": "/",
	}
)

type Typist struct {
	CountDownNumber int
	KBE             keybd_event.KeyBonding
	Delay           int
	Filename        string
	StringInput     []byte
}

func (t *Typist) ReadFile() (ba []byte, err error) {
	return ioutil.ReadFile(t.Filename)
}

func (t *Typist) Process(input []byte) {
	t.CountDown()
	for _, v := range input {
		vs := string(v)
		time.Sleep(time.Millisecond * time.Duration(t.Delay))
		if unicode.IsUpper(rune(v)) {
			t.Put(strings.ToLower(vs), true)
			continue
		}
		value, ok := switches[vs]
		if ok {
			t.Put(value, true)
			continue
		}
		switch vs {
		case "\t":
			t.Put("tab", false)
		case "\n":
			t.Put("newline", false)
		default:
			t.Put(vs, false)
		}
	}
}

func (t *Typist) Put(input string, shift bool) {
	t.KBE.SetKeys(keys[input])
	t.KBE.HasSHIFTR(shift)
	err := t.KBE.Launching()
	if err != nil {
		panic(err)
	}
}

func (t *Typist) CountDown() {
	for x := t.CountDownNumber; x > 0; x-- {
		fmt.Printf("\rStarting run in %d seconds  ", x)
		time.Sleep(time.Second)
	}
	fmt.Println("\rSending to keyboard          ")
}

func main() {
	delay := flag.Int("d", 5, "delay between keyboard events")
	inputFile := flag.String("f", "", "input file")
	stringInput := flag.String("s", "", "input string")
	countdown := flag.Int("c", 5, "start typing delay")
	flag.Parse()
	var err error
	t := Typist{
		CountDownNumber: *countdown,
		Delay:           *delay,
		Filename:        *inputFile,
		StringInput: func() []byte {
			if *stringInput != "" {
				return []byte(*stringInput)
			}
			return nil
		}(),
	}

	t.KBE, err = keybd_event.NewKeyBonding()
	if err != nil {
		fmt.Printf("ERROR::keybd_event.NewKeyBonding()::%v\n", err)
	}
	if t.Filename != "" {
		ba, err := t.ReadFile()
		if err != nil {
			log.Printf("ERROR::t.ReadFile(t.Filename)::%v\n", err)
		}
		t.Process(ba)
		return
	}
	if t.StringInput != nil {
		t.Process(t.StringInput)
		return
	}
	fmt.Println("USAGE: ghost-typist")
	flag.PrintDefaults()
}
